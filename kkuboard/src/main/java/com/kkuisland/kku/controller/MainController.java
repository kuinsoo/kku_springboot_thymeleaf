package com.kkuisland.kku.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;


/**
 * MainController
 */
@RestController
public class MainController {

   @RequestMapping(value="/", method=RequestMethod.GET)
   public String mainpage() throws Exception {
        System.out.println("Index");
       return "redirect:index";
   }
   @RequestMapping("/foo")
    public RedirectView handleFoo() {
    return new RedirectView("index");
   }
   
}