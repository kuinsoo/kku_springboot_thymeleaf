package com.kkuisland.kku.controller;

import javax.validation.Valid;

import com.kkuisland.kku.model.User;
// import com.kkuisland.kku.repository.UserRepository;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


/**
 * UserController
 */
@Controller
public class UserController {

   @GetMapping("/signup")
   public String showSignUpForm(User user) {
      return "add-user";
   }

   @PostMapping("/adduser")
   public String addUser(@Valid User user, BindingResult result, Model model) {
      if (result.hasErrors()) {
         return "add-user";
      }

      // userRepository.save();
      // model.addAttribute("user", userRepository.findAll());
      return "index";
   }

   // additional CRUD methods

   @GetMapping("/edit/{id}")
   public String showUpdateForm(@PathVariable("id") long id, Model model) {
      // User user = userRepository.findById(id)
         // .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
      
      // model.addAttribute("users", user);
      return "update-user";
   }

   @GetMapping("/delte/{id}")
   public String deleteUser(@PathVariable("id") long id, Model model) {
      // User user = userRepository.findById(id)
         // .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
      // userRepository.delete(user);
      // model.addAttribute("users", userRepository.findAll());
      return "index";
   }


}