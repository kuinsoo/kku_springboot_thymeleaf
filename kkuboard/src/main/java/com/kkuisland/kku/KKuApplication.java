package com.kkuisland.kku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KKuApplication {

	public static void main(String[] args) {
		SpringApplication.run(KKuApplication.class, args);
	}

}
