package com.kkuisland.kku.repository;

import com.kkuisland.kku.model.User;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * UserRepository
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

   // @Query("SELECT * FROM user WHERE id = :id")
   User findByUser(String email);   
   
   @Query("SELECT * FROM user WHERE LOWER(name) = LOWER(:name)")
   User retrieveByName(@Param("name") String name);

   // void save(User user);

}